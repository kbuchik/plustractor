**To do list for Google+ archiving project**

*Stuff I never got gone before the APIs went down*

- Add --update-threads option to loop through all activities (separate option for activities less than a month old) and add any new comments
- Add function for looking up a profile ID in the archival project spreadsheet by their display name
- Add per-account OAUTH token authorization and API access
- Figure out how to scrape non-public posts that were shared only with circles (hopefully can be done via OAUTH authentication)
- Figure out how to scrape groups
