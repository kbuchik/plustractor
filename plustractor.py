#!/usr/bin/python3

#
# plustractor.py -- A Python script for extracting data from the Google+ API into a standardized database format
#
# Author: Kevin Buchik <kdbuchik@gmail.com>
# Copyright October 2018
#

#
# This program became useless on March 6th, 2019, as that is when the Google+ APIs began returning 403s for every request.
# Besides a few minor code cleanups and typo corrections, this comment will be my final addition to this script.
# Peace out, Plustractor. All the time it took to write you sure paid off.
# -- Kevin Buchik, 4/30/2019
#

import datetime
import getopt
import json
import os
import random
import re
import requests
import shutil
import sqlite3
import sys
import threading
import time

# Modules needed for spreadsheet operations
import gspread
from oauth2client.service_account import ServiceAccountCredentials

### GLOBAL VARS ###

# API keys
apikeys = []                            # List of Google+ API keys to switch between
oauth_client_id = oauth_token = ''      # Google OAuth client ID and token strings

# File objects
db = None                               # SQLite database connection object
logger = None                           # Log text file object

# File paths
config_file = 'config.json'             # Name of the config JSON file (containing the API and other config options)
dbfile = ''                             # Path for the SQLite database file
dbname = 'plustractor.sqlite'           # Name (not path) of the SQLite database file
directory = 'gplus_archive'             # Path of the directory where data scraped during this run should be saved to
log_file = 'ps_log.txt'                 # Name for the log file

# Option flags
force = False                           # Force mode (if set, will always delete and update certain local records)
log_enabled = True                      # Logging enabled (disabled with --no-log option)
quiet = False                           # Quiet mode (suppresses most output messages, useful for batch scraping of profiles)
refresh_mode = False                    # When scraping profiles with the --archive option, use the refreshActivities() function instead of scrapeProfile()
verbosity = 0                           # Verbosity level (1 or 2 will print more status/debug messages)

# Regex for matching against G+ post/activity URLs
postUrlRegex = '^https?://plus\.google\.com/(u/0/)?([0-9]+|\+[a-zA-Z0-9]+)/posts/[a-zA-Z0-9]{11}$'

# Globals used during scraping
comment_dellog = []                     # List of comment IDs to delete after activity scraping
comment_savelog = {}                    # Dictionary of comment IDs/JSON blobs to save after activity scraping
person_savelog = {}                     # Dictionary of person IDs/JSON blobs to save after scraping a profile
comment_counter = 0                     # Count of comments archived during a profile scrape
activity_counter = 0                    # Counter for activities (currently used by --refresh-threads option only)
request_counter = 0                     # Count of how many successful requests were submitted to the G+ API

# List to store active threads (currently unused)
thread_spool = []

# Variables needed for GDC archiving project
project_dir_name = 'gdc_archive_final'  # Directory name for the GDC archive project
operator_name = ''                      # Name of the person running plustractor; defined in config file (needed for archive project)
doc = None                              # Google Sheets object
lastActivityCount = 0                   # Number of activities saved during the last profile scrape
maxIdRange = 100

# Config data for connecting to archive project spreadsheet
sheet_key = '1QDEDNTX6gsO9XyXuVvlbQlKjKyNZ1zSrtZzHnzrqiHk'
sheet_name = 'Google+ Archival Project Master Scrapelist'
oauth_keyfile = 'gsheets_client_secret.json'


### MAIN ###

def main():    
    global activity_counter,apikeys,comment_counter,db,dbfile,dbname,directory,force,log_enabled,oauth,quiet,refresh_mode,verbosity
    
    action = 0      # 0=none, 1=scrape profile activity, 2=scrape profile data, 3=scrape single activity, 4=name to person ID, 5=post URL to activity ID, 6=purge activities and comments for profile, 7=populate ID fields for spreadsheet, 8=archive a profile for the project
    
    personTarget = ''
    activityTarget = ''
    commentTarget = ''
    
    sheetRange = 1
    dateRange = ''
    
    # Load config file
    loadConfig()
    
    # Command argument parsing
    try:
        opts, args = getopt.getopt(sys.argv[1:], 'ha:p:s:v', ['activity=', 'apikey=', 'archive=', 'count-acts=', 'date=', 'db=', 'dir=', 'force', 'help', 'nametoid=', 'no-log', 'populate-ids', 'profile=', 'purge=', 'quiet', 'range=', 'refresh-acts=', 'rescrape-threads=', 'rf', 'scrape=', 'urltoid=', 'verbose', 'vv'])
    except getopt.GetoptError as err:
        print('Error: ', err)
        printHelp()
        exit(1)
    for opt, arg in opts:
        # Option arguments
        if opt == '-h' or opt == '--help':
            printHelp()
            exit(1)
        elif opt == '--apikey':
            apikeys.append(arg)
        elif opt == '--date':
            print("Date limit not implemented")
        elif opt == '--db':
            dbname = arg
        elif opt == '--dir':
            directory = arg
            if directory.endswith('/'):
                directory = directory[:-1]
        elif opt == '--force':
            force = True
        elif opt == '--no-log':
            log_enabled = False
        elif opt == '--quiet':
            quiet = True
            verbosity = 0
        elif opt == '--range':
            if arg.isdigit():
                rng = int(arg)
                if rng > 1 and rng <= maxIdRange:
                    sheetRange = rng
                else:
                    print("Error: argument to --range must be an integer between 2 and " + str(maxIdRange) + " (inclusive)")
                    exit(1)
            else:
                print("Error: argument to --range must be an integer between 2 and " + str(maxIdRange) + " (inclusive)")
                exit(1)
        elif opt == '--rf':
            refresh_mode = True
        elif opt == '-v' or opt == '--verbose':
            verbosity = 1
            if quiet:
                verbosity = 0
        elif opt == '--vv':
            verbosity = 2
            if quiet:
                verbosity = 0
        # Action arguments
        elif opt == '-s' or opt == '--scrape':
            action = 1
            personTarget = arg
            break
        elif opt == '-p' or opt == '--profile':
            action = 2
            personTarget = arg
            break
        elif opt == '-a' or opt == '--activity':
            action = 3
            activityTarget = arg
            break
        elif opt == '--nametoid':
            action = 4
            personTarget = arg
            break
        elif opt == '--urltoid':
            action = 5
            activityTarget = arg
            break
        elif opt == '--purge':
            action = 6
            personTarget = arg
            break
        elif opt == '--populate-ids':
            action = 7
            log_enabled = False
            break
        elif opt == '--archive':
            if not (arg.isdigit() and len(arg) == 21):
                print("Error: target of --archive must be a valid Google+ profile ID (21 digit integer)")
                exit(1)
            action = 8
            personTarget = arg
            directory = project_dir_name
            logging_enabled = True
            break
        elif opt == '--count-acts':
            action = 9
            personTarget = arg
            logging_enabled = False
            break
        elif opt == '--refresh-acts':
            action = 10
            personTarget = arg
            break
        elif opt == '--rescrape-threads':
            action = 11
            dateRange = arg
            break

    # Initialize directory structure and connect to the database file (initializing if it doesn't exist)
    setupDirectory(directory)
    dbfile = directory + '/' + dbname
    if not os.path.isfile(dbfile):
        initializeDB(dbfile)
        print("Initialized new database file " + dbfile)
        writeLog("Initialized database file " + dbfile)
    try:
        db = sqlite3.connect(dbfile)
        if not verifyDB(db):
            print(dbfile + " doesn't appear to contain valid plustractor schema (remove it or use a different directory path)")
            exitProg(2)
    except sqlite3.Error:
        print("Error connecting to database file " + dbfile)
        exit(2)
    
    # Action switch
    if action == 0:
        print("Usage: plustractor.py [OPTIONS] {-a [activity ID or URL] | -s [profile ID] | -p [profile ID]}\nMust supply one of the options -a, -p, or -s to specificy a target to scrape\nUse '--help' for more information")
        exitProg(1)
    
    elif action == 1:
        # Scrape an entire profile for all activity and comments
        pJson = getPersonData(personTarget, False)
        if pJson == 'ERROR 404':
            print("Error: Target profile \"" + personTarget + "\" not found")
            exitProg(3)
        pData = json.loads(pJson)
        persId = pData['id']
        scrapeProfile(persId)
    
    elif action == 2:
        # Scrape a profile's data only
        pJson = getPersonData(personTarget, False)
        if pJson == 'ERROR 404':
            print("Error: Profile not found")
            exitProg(3)
        pData = json.loads(pJson)
        persId = pData['id']
        if checkForObject(persId, 'people', db):
            deletePerson(persId)
            if verbosity > 0:
                print('Profile "' + persId + '" already in database; overwriting')
        savePersonData(pJson)
        saveProfileImages(pData)
        writeLog("\nProfile metadata for " + persId + " saved to database")
    
    elif action == 3:
        # Scrape an activity and all comments
        actId = activityTarget
        if re.match(postUrlRegex, activityTarget, re.I):
            actId = activityUrlToId(activityTarget)
            if actId == 'BADURL':
                print("URL format mismatch. A URL for a Google+ must conform to one of the following formats:")
                print("https://plus.google.com/[user ID or +ProfileName]/posts/[11 character shortcode]")
                print("https://plus.google.com/u/0/[user ID or +ProfileName]/posts/[11 character shortcode]")
                exitProg(1)
            elif actId == 'P404':
                print("Encountered a 404 error when trying to search user's activity feed for that post")
                exitProg(3)
            elif actId == 'A404':
                print("Unable to find that post in user's activity feed (double-check the URL)")
                exitProg(3)
        print("Attempting to pull activity data for ID " + actId)
        aJson = getActivityData(actId)
        if aJson == 'ERROR 404':
            print("Error: Activity/post not found")
            exitProg(3)
        aData = json.loads(aJson)
        print("Scraping all post, comment, and commenter data for activity " + actId)
        if checkForObject(actId, 'activities', db):
            deleteActivity(actId)
            #purgeActivityComments(actId)
            if verbosity > 0:
                print('Activity "' + actId + '" already in database; overwriting')
        opId = aData['actor']['id']
        if not checkForObject(opId, 'people', db):
            savePersonData(getPersonData(opId))
        elif force:
            deletePerson(opId)
            savePersonData(getPersonData(opId))
        scrapeActivity(actId)
        writeLog("\nActivity with ID " + actId + " scraped")
    
    elif action == 4:
        # Print the person ID given a +name (or display name, to be added later)
        persId = personNameToId(personTarget)
        if persId == '':
            if quiet:
                print("NOT FOUND")
            else:
                print("Couldn't match \"" + personTarget + "\" to an ID")
        elif persId == 'ERROR 403':
            print(persId)
        else:
            if quiet:
                print(persId)
            else:
                print("Person ID for user \"" + personTarget + "\" is " + persId)
    
    elif action == 5:
        # Get the activity ID of a post from its URL
        aId = activityUrlToId(activityTarget)
        if aId == 'BADURL':
            print("URL format mismatch. A URL for a Google+ must conform to one of the following formats: [TODO]")
        elif aId == 'P404':
            print("404 error when trying to fetch activity feed for that profile")
        elif aId == 'A404':    
            print("Unable to locate activity with URL \"" + activityTarget + "\" in poster's activity feed")
        else:
            print("Activity ID: " + aId)
    
    elif action == 6:
        # Purge all activities for a person
        print("Purging all activities and comments for account " + personTarget + " from database...")
        purgePersonActivities(personTarget)
    
    elif action == 7:
        # Populate all empty person ID fields in the spreadsheet
        setupSheets()
        populateAllPersonIDs()
    
    elif action == 8:
        # Scrape one or more profiles for the archival project
        setupSheets()
        if sheetRange > 1:
            archiveRange(personTarget, sheetRange)
        else:
            archiveProfile(personTarget)
    
    elif action == 9:
        actCount = countPersonActivities(personTarget)
        print("Activity count for " + personTarget + ":")
        print(str(actCount))

    elif action == 10:
        if not quiet:
            print("Attempting to get new/unsaved activities for " + personTarget);
        refreshActivities(personTarget)

    elif action == 11:
        dates = dateRange.split(':')
        start = dates[0]
        end = ''
        if len(dates) == 1:
            end = datetime.datetime.now().strftime('%Y-%m-%d')
        else:
            end = dates[1]
        print("Building list of activities/threads in database posted between " + start + " and " + end + "...")
        acts = DB_getActivitiesInTimeRange(start, end)
        numActs = len(acts)
        print("Found " + str(numActs) + " threads in date range")
        writeLog("Re-scraping " + str(numActs) + " activities dated from " + start + " to " + end)
        print("Re-scraping 0 of %d activities..." % numActs, end='')
        sys.stdout.flush()
        count = 0
        comment_counter = 0
        activity_counter = 0
        for act in acts:
            count += 1
            print("\rRe-scraping {0} of {1} activities...".format(str(count), str(numActs)), end='')
            sys.stdout.flush()
            scrapeActivity(act)
        print("\nRe-scrape of threads complete; {0} new comments archived from {1} threads".format(str(comment_counter), str(activity_counter)))
    
    # End program and exit
    print("plustractor finished run at " + getTimeStr() + " and sent " + str(request_counter) + " API requests")
    exitProg(0)
 
### FUNCTIONS ###

# Scrape a profile for the archive project and marked it as scraped in the spreadsheet 
def archiveProfile(persId):
    pJson = getPersonData(persId, False)
    if pJson == 'ERROR 404':
        print("Error: Profile with ID " + persId + " does not exist; double-check and remove from sheet if necessary")
        return False
    if refresh_mode:
        refreshActivities(persId)
        if lastActivityCount > 0:
            loc = getIDLocation(persId)
            sheet = doc.get_worksheet(loc[0])
            newcount = lastActivityCount + int(sheet.cell(loc[1], 7).value)
            sheet.update_cell(loc[1], 5, getTimeStr())
            sheet.update_cell(loc[1], 7, newcount)
            print("Completed activity refresh of profile \"" + sheet.cell(loc[1], 1).value + "\" (" + str(lastActivityCount) + " new activities)")
        else:
            print("Skipped " + persId + " (no new activities found)")
    else:
        scrapeProfile(persId)
        setupSheets()
        markAsScraped(persId, getTimeStr(), operator_name, lastActivityCount)
    return True;

def archiveRange(startId, num):
    idList = []
    loc = getIDLocation(startId)
    if loc == None:
        print("Error: Profile with ID " + startId + " does not exist in the spreadsheet")
        exit(4)
    sRow = loc[1] - 1
    idCol = doc.get_worksheet(loc[0]).col_values(3)
    if (sRow + num) > len(idCol) - 1:
        idList = idCol[sRow:]
    else:
        idList = idCol[sRow:sRow+num]
    for pId in idList:
        archiveProfile(pId)

# Make a call to the Google+ API and return the raw JSON reponse
# "call" should contain all of the request URL that follows "/plus/v1/" EXCLUDING parameters
# Any needed parameters should be passed to "prm" as a dictionary (except API key or OAuth tokens as they are added in this function)
# Function will exit with signal 3 on all HTTP errors other than 404, in which case it will return "ERROR 404"
def plusAPICall(call, prm = {}, retry404s = True):
    global apikeys,request_counter
    if prm == None:
        prm = {}
    if retry404s == None:
        retry404s = True
    if len(apikeys) == 0:
        print("All API keys have been removed (check quota and usage data)")
        exitProg(3)
    url = 'https://www.googleapis.com/plus/v1/' + call
    apikey = getRandomKey()
    prm['key'] = apikey
    req = requests.get(url, params=prm)
    request_counter += 1
    if req.status_code == 500:
        delay = 0
        while delay < 3:
            time.sleep(delay)
            req = requests.get(url, params=prm)
            request_counter += 1
            if req.status_code == 200:
                return req.text
            delay += 1
        print("The following request failed after 3 error 500 retries with status code %s: " % str(req.status_code))
        print(req.url)
        exitProg(3)
    if verbosity > 1:
        print("Submitting request: " + req.url)
    if req.status_code == 200:
        request_counter += 1
        return req.text
    elif req.status_code == 204:
        delay = 0.0
        while req.status_code == 204:
            delay += 0.1
            if delay > 1:
                print("Made 11 attempts to resolve 204 error, now exiting; request follows:")
                print(req.url)
                exit(4)
            time.sleep(delay)
            req = requests.get(url, params=prm)
        return req.text
    elif req.status_code == 400:
        print("Error: Bad request - suspected invalid API key; removing offending key: " + apikey)
        apikeys.remove(apikey)
        return plusAPICall(call, prm)
        #print("URL of request:\n" + req.url)
        exitProg(3)
    elif req.status_code == 403:
        if 'userRateLimitExceed' in req.text:
            return 'ERROR 403 RATE LIMITED'
        if 'dailyLimitExceeded' in req.text:
            # Remove the offending API key and try the request again
            print("Request returned a dailyLimitExceeded error; API key " + apikey + " removed from rotation")
            apikeys.remove(apikey)
            return plusAPICall(call, prm)
        if 'Forbidden' in req.text:
            # More Google+ shutdown shenanigans; retry a bunch of times
            delay = 0.0
            while req.status_code == 403:
                delay += 0.1
                if delay >= 5:
                    print("Exiting after making 11 attempts to resolve 403 Forbidden error on the following request:")
                    print(req.url)
                    exit(4)
                time.sleep(delay)
                req = requests.get(url, params=prm)
            return req.text
        else:
            print("HTTP request failed with a 403 error:")
            print(req.url)
            print("Response:")
            print(req.text)
            exitProg(3)
    elif req.status_code == 404:
        if not retry404s:
            return "ERROR 404"
        # G+ API is intermittently failing with 404 erros on certain requests, so we're going to retry all 404 errors (unless we're told not to)
        delay = -1
        while req.status_code == 404:
            delay += 1
            if delay >= 3:
                return 'ERROR 404'
            time.sleep(delay)
            req = requests.get(url, params=prm)
            request_counter += 1
        return req.text
    else:
        print("HTTP error: request returned unexpected status code %s; request url:" % str(req.status_code))
        print(req.url)
        print("Request response:")
        print(req.text)
        exitProg(3)

## API scraping "helper" functions

# Scrapes a profile's data and all of its activity after "date" and saves it to the database (including all comments and profiles of commenters)
# If no date is specified, pulls all activity to the beginning of time
def scrapeProfile(persId):
    global comment_counter, lastActivityCount
    comment_counter = 0
    failedActs = []
    # First, get the profile's data
    pJson = getPersonData(persId)
    pData = json.loads(pJson)
    print("Initiating scrape of profile \"" + pData['displayName'] + "\" [ID # " + persId + "] at " + getTimeStr())
    writeLog("Initiated scrape of profile \"" + pData['displayName'] + "\" (ID: " + persId + ")")
    if not checkForObject(persId, 'people', db):
        savePersonData(pJson)
        saveProfileImages(pData)
    # Get the list of the person's activities
    activities = getPersonActivities(persId)
    writeLog(str(len(activities)) + " activities discovered for " + pData['displayName'])
    if not quiet:
        print("Found " + str(len(activities)) + " activities for " + pData['displayName'])
        print("Scraping activity data (0/%d)..." % len(activities), end='')
        sys.stdout.flush()
    person_savelog.clear()
    count = savedActs = 0
    totalActs = len(activities)
    # Iterate through list and scrape each activity
    for act in activities:
        count += 1
        if not quiet:
            print("\rScraping activity data ({0}/{1})...".format(str(count), str(totalActs)), end='')
            sys.stdout.flush()
        if checkForObject(act, 'activities', db):
            continue
        else:
            if scrapeActivity(act):
                savedActs += 1
            else:
                failedActs.append(act)
    # Process person save queue
    personTotal = len(set(person_savelog))
    if not quiet:
        print("\nSaving profile data for 0 of %d new commenters..." % personTotal, end='')
        sys.stdout.flush()
    personCount = 0
    for person in set(person_savelog):
        if person_savelog[person] == '':
            pJson = getPersonData(person)
            if pJson != 'ERROR 403':
                savePersonData(pJson)
            else:
                print("403 error on getting person data for " + person + " (shouldn't happen!)")
        elif person != '404':
            savePersonData(person_savelog[person])
        personCount += 1
        if not quiet:
            print("\rSaving profile data for {0} of {1} new commenters...".format(str(personCount), str(personTotal)), end='')
            sys.stdout.flush()
    person_savelog.clear()
    writeLog("Completed scrape of profile " + persId + " (added " + str(totalActs) + " activities, " + str(comment_counter) + " comments, " + str(personTotal) + " new profiles)\n")
    print("\nScrape of profile # {0} complete; archived {1} (out of {2}) new activities, {3} new comments and {4} new commenter profiles".format(persId, str(savedActs), str(totalActs), str(comment_counter), str(personTotal)))
    lastActivityCount = totalActs

# Runs through the given person's activity feed until it finds an activity already in the database, then scrapes those activities
def refreshActivities(persId):
    global lastActivityCount
    comment_counter = 0;
    if not checkForObject(persId, 'people', db):
        print("Error: --refresh-acts can only be used on a profile already in the database (try using --scrape first)")
        return False
    actList = []
    reqstr = 'people/' + persId + '/activities/public'
    pageJson = plusAPICall(reqstr)
    if pageJson == 'ERROR 404':
        print("The profile with ID " + persID + " exists in the database, but could not be found on G+")
        return False
    pageData = json.loads(pageJson)
    npt = 'nextPageToken'
    stopSearch = False
    while not stopSearch:
        for act in pageData['items']:
            if not stopSearch:
                if not checkForObject(act['id'], 'activities', db):
                    actList.append(act['id'])
                else:
                    stopSearch = True
        if npt in pageData and not stopSearch:
            nextPage = pageData[npt]
            pageJson = plusAPICall(reqstr, {'pageToken': nextPage})
            pageData = json.loads(pageJson)
        else:
            stopSearch = True
    print("Found " + str(len(actList)) + " new activities for profile " + persId)
    lastActivityCount = 0
    for act in actList:
        if scrapeActivity(act):
            lastActivityCount += 1
    return True

# Improved version of scrapeActivity() that doesn't make redundant API calls for each individual comment
def scrapeActivity(actId):
    global comment_counter, activity_counter
    newCommentFlag = False
    aJson = getActivityData(actId)
    if aJson == 'ERROR 404':
        print("Attempt to scrape activity \"" + actId + "\" returned a 404 error")
        return False
    aData = json.loads(aJson)
    if checkForObject(actId, 'activities', db):
        deleteActivity(actId)
    saveActivityData(aJson)
    # Create a dictionary of comments to save (keys are the comment IDs)
    comments = {}
    # Request the first page of the comment feed for this activity
    reqstr = 'activities/' + actId + '/comments'
    feedJson = plusAPICall(reqstr)
    if feedJson == 'ERROR 404':
        print("Attempt to get comment feed for activity \"" + actId + "\" returned a 404 error")
        return False
    npt = 'nextPageToken'
    feedData = json.loads(feedJson)
    nextToken = ''
    # Iterate through feed pages and save comments to dictionary
    while True:
        for com in feedData['items']:
            comments[com['id']] = json.dumps(com)
        if npt in feedData:
            nextToken = feedData[npt]
            feedJson = plusAPICall(reqstr, {'pageToken': nextToken})
            feedData = json.loads(feedJson)
        else:
            break
    # Iterate through the dictionary of comment data and save it
    for cmtId in comments:
        if checkForObject(cmtId, 'comments', db):
            if force:
                comment_counter += 1
                deleteComment(cmtId)
                saveCommentData(comments[cmtId])
        else:
            newCommentFlag = True
            comment_counter += 1
            saveCommentData(comments[cmtId])
        # Request and save the comment poster's profile data only if it's not in the database and hasn't been save on this run
        pId = json.loads(comments[cmtId])['actor']['id']
        if not checkForObject(pId, 'people', db) and pId not in person_savelog:
            pJson = getPersonData(pId)
            if pJson == 'ERROR 404':
                print("404 error requesting data for person " + pId)
            elif pJson == 'ERROR 403':
                print("403 error requesting data for person " + pId)
                person_savelog[pId] = ''
            else:
                person_savelog[pId] = pJson
    if newCommentFlag:
        activity_counter += 1
    return True

# Threaded version of scrapeActivity that parallelizes collection of comment data
# Currently unused/deprecated; scrapeActivity() is more efficient (partly because Google's APIs do NOT like parallel queries)
def threadedScrapeActivity(actId):
    global save_comment_queue, delete_comment_queue, thread_spool
    # Get and save the activity data
    aJson = getActivityData(actId)
    if aJson == 'ERROR 404':
        print("Attempt to scrape activity \"" + actId + "\" returned a 404 error")
        return False
    aData = json.loads(aJson)
    if checkForObject(actId, 'activities', db):
        deleteActivity(actId)
    saveActivityData(aJson)
    # Clear the queue lists
    comment_dellog.clear()
    comment_savelog.clear()
    # Get the list of comment IDs under this activity
    comments = getActivityComments(actId)
    if verbosity > 0:
        print("Saving " + str(len(comments)) + " comments for activity " + actId)
    # Initiate threaded process
    thread_spool.clear()
    if len(comments) == 0:
        return True
    for cmtId in comments:
        proc = threading.Thread(target=commentLoad, args=[cmtId])
        proc.start()
        thread_spool.append(proc)
    for proc in thread_spool:
        proc.join()
    # Process delete queue
    for cmtId in comment_dellog:
        deleteComment(cmtId)
    comment_dellog.clear()
    # Process comment save queue
    for comment in set(comment_savelog):
        if comment_savelog[comment] == '':
            cJson = getCommentData(comment)
            pId = json.loads(cJson)['actor']['id']
            person_savelog[pId]
            saveCommentData(cJson)
        else:
            saveCommentData(comment_savelog[comment])
    comment_savelog.clear()
    return True

# Thread worker function to get data for a comment and save its poster's ID to a queue list
# Called by threads spawned in threadedScrapeActivity()
def commentLoad(cmtId):
    #global save_comment_queue, delete_comment_queue
    global comment_dellog, comment_savelog, person_savelog
    t_DB = sqlite3.connect(dbfile)      # Thread-specific database connection
    err403 = 'ERROR 403 RATE LIMITED'
    try:
        if checkForObject(cmtId, 'comments', t_DB):
            # If the comment exists in the database, then mark it for deletion and saving if --force is set, otherwise return
            if force:
                comment_dellog.append(cmtId)
                comment_savelog[cmtId] = getCommentData(cmtId)
            else:
                return False
        cmtJson = getCommentData(cmtId)
        if cmtJson == err403:
            # If we were rate-limited by the API, record the comment as an empty string, we'll get it sequentially later
            comment_savelog[cmtId] = ''
            return False
        elif cmtJson == 'ERROR 404':
            print("404 error on requesting comment " + cmtId)
            return False
        else:
            comment_savelog[cmtId] = cmtJson
        pId = json.loads(cmtJson)['actor']['id']
        if pId not in person_savelog and not checkForObject(pId, 'people', t_DB):
            pJson = getPersonData(pId)
            if pJson == 'ERROR 404':
                print("404 error on requesting person " + pId)
                person_savelog[pId] = '404'
            elif pJson == err403:
                # Attempt 3 times to request commenter data
                delay = 1
                while delay <= 3:
                    time.sleep(delay)
                    pJson = getPersonData(pId)
                    if pJson == err403:
                        delay += 1
                    else:
                        person_savelog[pId] = pJson
                        break
                person_savelog[pId] = ''
    except Exception as ex:
            print("While processing comment " + cmtId + "; encountered exception: " + str(ex))
    finally:
        t_DB.close()
    return True

# Returns a list of activity IDs for all of the specified person's public activities
def getPersonActivities(persId):
    activities = []
    reqstr = 'people/' + persId + '/activities/public'
    listJson = plusAPICall(reqstr)
    if listJson == 'ERROR 404':
        return activities
    npt = 'nextPageToken'
    listData = json.loads(listJson)
    nextPage = ''
    pages = 1
    if not quiet:
        print("Retrieving list of activities.", end='')
        sys.stdout.flush()
    while True:
        # G+ API is now occasionally returning empty activity feed pages; retry these fails up to 10 times
        if len(listData['items']) == 0:
            retries = 1
            while len(listData['items']) == 0 and retries < 10:
                time.sleep(0.5)
                listJson = plusAPICall(reqstr, {'pageToken': nextPage})
                listData = json.loads(listJson)
                retries += 1
        for act in listData['items']:
            activities.append(act['id'])
        if npt in listData:
            nextPage = listData[npt]
            listJson = plusAPICall(reqstr, {'pageToken': nextPage})
            listData = json.loads(listJson)
        else:
            break
        if not quiet:
            print('.', end='')
            sys.stdout.flush()
        pages += 1
    if not quiet:
        print()
    return activities

# Returns a count of a person's activites
def countPersonActivities(persId):
    return len(getPersonActivities(persId))

# Returns a list of comment IDs for all comments under the specified activity
def getActivityComments(actId):
    comments = []
    reqstr = 'activities/' + actId + '/comments'
    listJson = plusAPICall(reqstr)
    if listJson == 'ERROR 404':
        return comments
    npt = 'nextPageToken'
    listData = json.loads(listJson)
    nextPage = ''
    while True:
        for com in listData['items']:
            comments.append(com['id'])
        if npt in listData:
            nextPage = listData[npt]
            listJson = plusAPICall(reqstr, {'pageToken': nextPage})
            listData = json.loads(listJson)
        else:
            break
    return comments

# Given a users's short name ("+Name"), returns their person ID (or empty string if not found)
# The preceding plus can be omitted
def personNameToId(name):
    pJson = getPersonData(name)
    if pJson == 'ERROR 404':
        return ''
    elif pJson == 'ERROR 403 RATE LIMITED':
        return 'ERROR 403'
    else:
        pData = json.loads(pJson)
        return pData['id']

# Given the full URL of a G+ post, returns the activity ID for that post via brute force search of the user profile
# (because Google is retarded and doesn't allow the ID to be determined directly)
def activityUrlToId(url):
    if not re.match(postUrlRegex, url, re.I):
        return 'BADURL'
    parts = url.split('/')
    #if len(parts) < 5:
    #    return 'BADURL'
    #if (parts[0] != 'https:' or parts[0] != 'http:') or (parts[2] != 'plus.google.com' or parts[4] != 'posts'):
    #    return 'BADURL'
    pId = parts[3]
    if pId == 'u':
        pId = parts[5]
        url = 'https://plus.google.com/' + pId + '/posts/' + parts[7]
    #print("[DEBUG] Attempting lookup on person: " + pId)
    reqstr = 'people/' + pId + '/activities/public'
    actListJson = plusAPICall(reqstr)
    if actListJson == 'ERROR 404':
        return 'P404'
    npt = 'nextPageToken'
    actFeed = json.loads(actListJson)
    while True:
        for act in actFeed['items']:
            if act['url'] == url:
                return act['id']
        if npt in actFeed:
            actFeed = json.loads(plusAPICall(reqstr, {'pageToken': actFeed[npt]}))
        else:
            break
    return 'A404'

## API object get/save functions (for activities, comments, and people)

# Gets the JSON data for a G+ "activity" object (a post or share) using the API
def getActivityData(actId):
    act = plusAPICall('activities/' + actId)
    return act

# Save an activity object to the database using the JSON obtained with getActivityData()
def saveActivityData(activityJson):
    # Deserialize json object
    a = json.loads(activityJson)
    actId = a['id']
    
    # Construct property dictionary
    props = {
        'id' : actId,
        'etag' : a['etag'],
        'scrapeTime' : getTimeStr(),
        'title' : None,
        'published' : a['published'],
        'updated' : a['updated'],
        'actorId' : a['actor']['id'],
        'actor' : json.dumps(a['actor']),
        'rawContent' : a['object']['content'],
        'originalContent' : None,
        'verb' : a['verb'],
        'object' : json.dumps(a['object']),
        'annotation' : None,
        'crosspostSource' : None,
        'provider' : a['provider']['title'],
        'access' : None,
        'geocode' : None,
        'address' : None,
        'radius' : None,
        'placeId' : None,
        'placeName' : None,
        'location' : None,
        'rawJson': activityJson
    }
    
    # Load remaining values into dictionary
    for pr in props:
        if pr in ('id','etag','scrapeTime','published','actorId','actor','verb','object','rawContent','updated','provider','rawJson'):
            continue
        elif pr == 'originalContent' and 'originalContent' in a['object']:
            props[pr] = a['object']['originalContent']
        elif pr in ('access','attachments','location') and pr in a:
            props[pr] = json.dumps(a[pr])
        else:
            if pr in a:
                props[pr] = a[pr]
    
    # Insert object into database
    insertPropertyDict(props, 'activities')

# Gets the JSON data for a G+ "comment" object (a single comment under an activity) using the API
def getCommentData(commentId):
    return plusAPICall('comments/' + commentId)

# Save a comment object to the database using the JSON obtained with getCommentData()
def saveCommentData(commentJson):
    # Deserialize json object
    c = json.loads(commentJson)
    comId = c['id']
    
    # Construct property dictionary
    props = {
        'id' : comId,
        'etag' : c['etag'],
        'scrapeTime' : getTimeStr(),
        'published' : c['published'],
        'updated' : None,
        'actorId' : c['actor']['id'],
        'actor' : json.dumps(c['actor']),
        'replyToId' : c['inReplyTo'][0]['id'],
        'replyToUrl' : c['inReplyTo'][0]['url'],
        'rawContent' : c['object']['content'],
        'originalContent' : None,
        'selfLink' : c['selfLink'],
        'plusoners' : c['plusoners']['totalItems']
    }
    
    # Load the other values (no need for loop as there are only two)
    if 'updated' in c:
        props['updated'] = c['updated']
    if 'originalContent' in c['object']:
        props['originalContent'] = c['object']['originalContent']
    
    # Insert object into database
    insertPropertyDict(props, 'comments')

# Given the ID of a comment in the database, recreates the JSON from the request response (as comment JSON data isn't saved)
def rebuildCommentJson(cmtId):
    comment = getDatabaseObject(cmtId, 'comments')
    if comment == None:
        return ''
    
    # Rebuild request dict
    cjson = {}
    cjson['kind'] = "plus#comment"
    cjson['etag'] = comment['etag']
    cjson['verb'] = "post"
    cjson['id'] = cmtId
    cjson['published'] = comment['published']
    if comment['updated'] != None:
        cjson['updated'] = comment['updated']
    cjson['actor'] = json.loads(comment['actor'])
    cjson['object'] = {'objectType' : "comment", 'content' : comment['rawContent']}
    if comment['originalContent'] != None:
        cjson['object']['content'] = comment['originalContent']
    cjson['selfLink'] = comment['selfLink']
    cjson['inReplyTo'] = [{'id' : comment['replyToId'], 'url' : comment['replyToUrl']}]
    cjson['plusoners'] = {'totalItems' : int(comment['plusoners'])}
    
    return json.dumps(cjson)

# Gets the JSON data for a G+ "person" object (i.e. a user's profile) using the API
# "person" can be either an ID string (consisting only of integers) or a display name (prepended with '+', which will be done automatically if not present)
def getPersonData(person, retry404s = True):
    if retry404s == None:
        retry404s = True
    if not person.isdigit() and person[0] != '+':
        person = '+' + person
    response = plusAPICall('people/' + person, {}, retry404s)
    return response

# Save a person's profile to the database using the JSON obtained with getPersonData()
def savePersonData(pJson):
    # Deserialize json object
    p = json.loads(pJson)
    persId = p['id']
    
    # Construct property dictionary
    props = {
        'id' : persId,
        'etag' : p['etag'],
        'scrapeTime' : getTimeStr(),
        'displayName' : p['displayName'],
        'nickname' : None,
        'name' : None,
        'birthday' : None,
        'gender' : None,
        'url' : None,
        'imageUrl' : None,
        'aboutMe' : None,
        'relationship' : None,
        'urls' : None,
        'organizations' : None,
        'placesLived' : None,
        'tagline' : None,
        'braggingRights' : None,
        'plusOneCount' : None,
        'circledByCount' : 0,
        'verified' : False,
        'cover' : None,
        'language' : None,
        'ageRange' : None,
        'emails' : None,
        'domain' : None,
        'occupation' : None,
        'skills' : None,
        'rawJson' : pJson
    }
    
    # Load remaining values into dictionary
    for pr in sorted(props):
        # Skip required values which have already been loaded
        if pr in ('id','etag','scrapeTime','displayName','circledByCount','verified','rawJson'):
            continue
        # Handle the special case of "imageUrl"
        if pr == 'imageUrl' and 'image' in p and 'url' in p['image']:
            props[pr] = p['image']['url']
        # Handle the special case of "ageRange"
        if pr == 'ageRange' and 'ageRange' in p:
            props[pr] = str(p['ageRange']['min'] + '-' + p['ageRange']['max'])
        # Handle values that require re-serializing the json blob
        elif pr in ('name','urls','organizations','placesLived','cover') and pr in p:
            props[pr] = json.dumps(p[pr])
        # Load all other values:
        else:
            if pr in p:
                props[pr] = p[pr]
    
    # Download profile and cover photos (threaded)
    # deprecated (no longer downloading images in this function)
    #proc = threading.Thread(target=saveProfileImages, args=[p])
    #proc.start()
    #proc.join()
    
    # Old (serial) code for saving pictures
    '''
    if props['imageUrl'] != None:
        profileImgPath = directory + '/profile_images/' + persId + '_profile.jpg'
        if not saveUrlToFile(props['imageUrl'], profileImgPath):
            print("Error retrieving and saving profile photo for ID " + persId)
    if props['cover'] != None:
        coverImgPath = directory + '/cover_images/' + persId + '_cover.jpg'
        coverJs = json.loads(props['cover'])
        if not saveUrlToFile(coverJs['coverPhoto']['url'], coverImgPath):
            print("Error retrieving and saving cover photo for ID " + persId)
    '''
    
    # Insert object into database
    insertPropertyDict(props, 'people')
    
    # Unused null-lambda-map code
    #nulls = lambda n: [None for _ in range(n)]
    #nickname, name, birthday, gender, url, imageUrl, aboutMe, relationship, urls, organizations, placesLived, tagline, braggingRights, plusOneCount, coverPhotoPath, coverPhotoData, language, ageRange, emails, domain, occupation, skills = nulls(22)

# Given the (deserialized) JSON data of a person request, downloads that person's profile and cover images into the appropriate folder
def saveProfileImages(pData):
    persId = pData['id']
    imageUrl = coverUrl = None
    if 'image' in pData and 'url' in pData['image']:
        imageUrl = pData['image']['url']
    if 'cover' in pData and 'coverPhoto' in pData['cover']:
        coverUrl = pData['cover']['coverPhoto']['url']
    if imageUrl != None:
        profileImgPath = directory + '/profile_images/' + persId + '_profile.jpg'
        if not os.path.isfile(profileImgPath):
            if not saveUrlToFile(imageUrl, profileImgPath):
                print("Error retrieving and saving profile photo for ID " + persId)
    if coverUrl != None:
        coverImgPath = directory + '/cover_images/' + persId + '_cover.jpg'
        if not os.path.isfile(coverImgPath):
            if not saveUrlToFile(coverUrl, coverImgPath):
                print("Error retrieving and saving cover photo for ID " + persId)

## Misc database access funcctions

# Returns the count of objects with the given ID in the given table (with 0 being counted as false)
# Will always return 0 when an invalid table name is passed
# "dbc" is the database connection (needed because this function is called from inside theads). If none is passed, will use the global connection "db"
def checkForObject(objId, table, dbc = db):
    if dbc is None:
        dbc = db
    if table not in ('activities','comments','people'):
        return 0
    c = dbc.cursor()
    c.execute("SELECT COUNT(*) FROM " + table + " WHERE id=?", (objId,))
    return c.fetchone()[0]

# Returns a number count of how many activities created by the given person ID are stories in the database
def DB_countPersonActivities(personId):
    c = db.cursor()
    c.execute("SELECT COUNT(*) FROM activities WHERE actorId=?", (personId,))
    return c.fetchone()[0]

# Returns a list of all activity records in the database whose "published" field is newer than "startDate" but older than "endDate" (inclusive)
# "person" is an option parameter which, if set, will restrict the results to activities posted by the given person ID
# startDate and endDate MUST be in YYYY-MM-DD format
def DB_getActivitiesInTimeRange(startDate, endDate, person=''):
    #qprm = tuple()
    if person is None:
        person = ''
    if person is '':
        query = "SELECT id FROM activities WHERE published >= '" + startDate + "' AND published <= '" + endDate + "'"
    else:
        query = "SELECT id FROM activities WHERE published >= '" + startDate + "' AND published <= '" + endDate + "'" + " AND actorId=" + person
    c = db.cursor()
    c.execute(query)
    result = c.fetchall()
    return [row[0] for row in result]

## Record deletion functions

# Removes the specified activity record from the database, but preserves comments
def deleteActivity(activityId):
    db.execute("DELETE FROM activities WHERE id=?", (activityId,))
    db.commit()

# Removes all comments from the database associated with the given activity, but not the activity record itself
def purgeActivityComments(activityId):
    db.execute("DELETE FROM comments WHERE replyToId=?", (activityId,))
    db.commit()

# Removes the specified comment from the database
def deleteComment(commentId):
    db.execute("DELETE FROM comments WHERE id=?", (commentId,))
    db.commit()

# Removes the specified person from the database (metadata only; preserves all activities and comments)
# Will also remove picture and cover images from local directory
def deletePerson(personId):
    db.execute("DELETE FROM people WHERE id=?", (personId,))
    db.commit()
    profileImg = directory + '/profile_images/' + personId + '_profile.jpg'
    coverImg = directory + '/cover_images/' + personId + '_cover.jpg'
    if os.path.isfile(profileImg):
        os.remove(profileImg)
    if os.path.isfile(coverImg):
        os.remove(coverImg)

# Removes all activities and comments associated with the given person (will NOT remove commenter profile data -- no way to do this safely)
def purgePersonActivities(personId):
    if checkForObject(personId, 'people', db):
        activities = DB_getPersonActivities(personId)
        db.execute("DELETE FROM activities WHERE actorId=?", (personId,))
        for act in activities:
            db.execute("DELETE FROM comments WHERE replyToId=?", (act,))
        db.commit()

## SQL database access functions

# Given the ID of a activity, comment, or person in the database plus the corresponding table name, returns a dictionary representing the row for that record
# Returns None if the specified item ID doesn't exist in the given table
def getDatabaseObject(objId, table, dbc = db):
    if dbc == None:
        dbc = db
    if table not in ('activities','comments','people'):
        return None
    dbc.row_factory = sqlite3.Row
    c = dbc.cursor()
    c.execute("SELECT * FROM " + table + " WHERE id=?", (objId,))
    result = [dict(row) for row in c.fetchall()]
    if len(result) > 0:
        return result[0]
    else:
        return None

# Given the ID of a person in the database, returns a list of IDs for all activities created by that person in the database
# Returns an empty list if the specified person ID does not exist in the database
def DB_getPersonActivities(personId):
    c = db.cursor()
    c.execute("SELECT id FROM activities WHERE actorId=?", (personId,))
    result = c.fetchall()
    return [row[0] for row in result]

# Given the ID of a person in the database, returns a list of IDs for all comments that person posted which are stored in the database
# Returns an empty list if the specified person ID does not exist in the database
def DB_getPersonComments(personId): 
    c = db.cursor()
    c.execute("SELECT id FROM comments WHERE actorId=?", (personId,))
    result = c.fetchall()
    return [row[0] for row in result]

# Given the ID of an activity in the database, returns a list of IDs for all comments under that activity in the database
# Returns an empty list if the specified activity ID does not exist in the database
def DB_getActivityComments(activityId):
    c = db.cursor()
    c.execute("SELECT id FROM comments WHERE replyToId=?", (activityId,))
    result = c.fetchall()
    return [row[0] for row in result]

## Database functions

# Initializes a new SQLite database with the Plustractor schema
def initializeDB(dbfile):
    if os.path.isfile(dbfile):
        raise ValueError("Error initializing database (the file %s already exists" % dbfile)
        exit(1)
    try:
        db = sqlite3.connect(dbfile)
        # Create people table
        db.execute('''CREATE TABLE people (id text PRIMARY KEY, etag text NOT NULL, scrapeTime datetime NOT NULL, displayName text NOT NULL, nickname text, name text, birthday text, gender text, url text, imageUrl text, aboutMe text, relationship text, urls text, organizations text, placesLived text, tagline text, braggingRights text, plusOneCount int, circledByCount int NOT NULL, verified boolean NOT NULL, cover text, language text, ageRange text, emails text, domain text, occupation text, skills text, rawJson text) WITHOUT ROWID''')
        # Create activities table
        db.execute('''CREATE TABLE activities (id text PRIMARY KEY, etag text NOT NULL, scrapeTime datetime NOT NULL, title text, published datetime NOT NULL, updated datetime NOT NULL, actorId text NOT NULL, actor text NOT NULL, rawContent text NOT NULL, originalContent text, verb text NOT NULL, object text NOT NULL, annotation text, crosspostSource text, provider text, access text, geocode text, address text, radius text, placeId text, placeName text, location text, rawJson text) WITHOUT ROWID''')
        # Create comments table
        db.execute('''CREATE TABLE comments (id text PRIMARY KEY, etag text NOT NULL, scrapeTime datetime NOT NULL, published datetime NOT NULL, updated datetime, actorId text NOT NULL, actor text, replyToId text NOT NULL, replyToUrl text NOT NULL, rawContent text NOT NULL, originalContent text, selfLink text, plusoners text) WITHOUT ROWID''')
    except sqlite3.Error as ex:
        print("Database error: " + ex)
    finally:
        db.close()

# Returns true if the database connection "db" has the correct schema
def verifyDB(db):
    column_ref = [
            "['id', 'etag', 'scrapeTime', 'displayName', 'nickname', 'name', 'birthday', 'gender', 'url', 'imageUrl', 'aboutMe', 'relationship', 'urls', 'organizations', 'placesLived', 'tagline', 'braggingRights', 'plusOneCount', 'circledByCount', 'verified', 'cover', 'language', 'ageRange', 'emails', 'domain', 'occupation', 'skills', 'rawJson']",
            "['id', 'etag', 'scrapeTime', 'title', 'published', 'updated', 'actorId', 'actor', 'rawContent', 'originalContent', 'verb', 'object', 'annotation', 'crosspostSource', 'provider', 'access', 'geocode', 'address', 'radius', 'placeId', 'placeName', 'location', 'rawJson']",
            "['id', 'etag', 'scrapeTime', 'published', 'updated', 'actorId', 'actor', 'replyToId', 'replyToUrl', 'rawContent', 'originalContent', 'selfLink', 'plusoners']"
    ]
    i = 0
    for tbl in ['people', 'activities', 'comments']:
        cur = db.execute('SELECT * FROM ' + tbl)
        colnames = list(map(lambda x: x[0], cur.description))
        if str(colnames) != column_ref[i]:
            return False
        i = i + 1
    return True

# Given a filled-out property dictionary and table name, inserts a new row those values into the database
# Called by the "saveXData()" functions
def insertPropertyDict(props, tblname):
    colnames = ','.join(sorted(props))
    vals = []
    for key in sorted(props):
        vals.append(props[key])
    sql = 'INSERT INTO ' + tblname + '(' + colnames + ') VALUES(' + ','.join('?' * len(vals)) + ')'
    try:
        db.execute(sql, tuple(vals))
        db.commit()
    except sqlite3.Error as ex:
        print('Database error: ' + str(ex))
        print('SQL query: ' + sql)
        exitProg(2)

## Time functions

# Returns the current date and time in the format YYYY-mm-dd HH:MM:SS
def getTimeStr():
    return datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

# Converts Google's timestamp format to the "YYYY-mm-dd HH:MM:SS" format used by plustractor functions
def convertGoogleTime(gtime):
    parts = gtime.split('T')
    return parts[0] + ' ' + parts[1].split('.')[0]

# Returns true if timeA is earlier than timeB
# Parameters must be strings in the format "YYYY-mm-dd HH:MM:SS" or "%Y-%m-%d %H:%M:%s"
# If timeB is omitted then timeA will be compared with the present time
def compareTimes(timeA, timeB=getTimeStr()):
    utimeA = datetime.datetime.strptime(timeA, '%Y-%m-%d %H:%M:%S').timestamp()
    utimeB = datetime.datetime.strptime(timeB, '%Y-%m-%d %H:%M:%S').timestamp()
    return (utimeA < utimeB)

## Google Sheets functions

# Initialize connection to spreadsheet
def setupSheets():
    global doc
    scope = ['https://spreadsheets.google.com/feeds']
    try:
        creds = ServiceAccountCredentials.from_json_keyfile_name(oauth_keyfile, scope)
        client = gspread.authorize(creds)
        doc = client.open_by_key(sheet_key)
    except gspread.exceptions.GSpreadException as ex:
        print("Error connecting to spreadsheet: " + str(ex))
        return False
    return True

# Returns a tuple indicating the location of a given user ID in the doc: (sheet#, row#)
# Returns None if the ID doesn't exist in the spreadsheet
def getIDLocation(query):
    sheetId = 0
    for sh in doc.worksheets():
        rowId = 1
        idCol = sh.col_values(3)
        for pId in idCol:
            if pId == query:
                nameCol = sh.col_values(1)
                return (sheetId, rowId)
            rowId += 1
        sheetId += 1
    return None

# Fills in the "Scraped At" and "Scraped By" columns in the row for the given person ID
def markAsScraped(persId, scrapedAt, scrapedBy="", activityCount=0):
    loc = getIDLocation(persId)
    if loc == None:
        return False
    sheet = doc.get_worksheet(loc[0])
    sheet.update_cell(loc[1], 5, scrapedAt)
    sheet.update_cell(loc[1], 6, scrapedBy)
    sheet.update_cell(loc[1], 7, activityCount)
    name = sheet.cell(loc[1], 1).value
    print('Marked "' + name + '" as scraped')
    return True

# Iterate through all rows and populate the ID column whenever an empty ID cell is found
# (either from the given URL, or by looking up the name with the G+ API)
def populateAllPersonIDs():
    print("Populating all empty person ID fields")
    for shId in range(0,3):
        sh = doc.get_worksheet(shId)
        nameCol = sh.col_values(1)
        urlCol = sh.col_values(2)
        idCol = sh.col_values(3)
        for rowId in range(0, len(nameCol)):
            persId = ''
            if rowId < len(idCol):
                persId = idCol[rowId]
            if rowId == 0:
                continue
            if persId == '':
                urlSuffix = urlCol[rowId].split('/')[3]
                if urlSuffix[0] != '+':
                    print("Populating ID entry for \"" + nameCol[rowId] + "\" with ID from URL: " + urlSuffix)
                    sh.update_cell(rowId + 1, 3, urlSuffix)
                else:
                    newId = personNameToId(urlSuffix)
                    if newId != '':
                        print("Populating ID entry for \"" + nameCol[rowId] + "\" with new ID: " + newId)
                        sh.update_cell(rowId + 1, 3, newId)
                    else:
                        print("Unable to get new ID for person \"" + nameCol[rowId] + "\"")

## Other functions

# Returns a specific API key from the stored list
# (if an invalid or out-of-range index is passed, the first key will be returned)
def getKeyByIndex(index=0):
    if not isinstance(index, int) or index < 0 or index >= len(apikeys):
        return apikeys[0]
    else:
        return apikeys[index]

# Returns a random API key from the stored list
def getRandomKey():
    return apikeys[random.randint(0, len(apikeys) - 1)]

# Loads API and OAuth keys from key file (for security reasons)
def loadConfig():
    global apikeys,oauth_client_id,operator_name
    conf = json.loads(open(config_file, 'r').read())
    apikeys = conf['apikeys']
    if len(apikeys) == 0 or apikeys[0] == '':
        print("Error: plustractor requires at least one API key in " + config_file + " in order to work")
        exit(1)
    oauth_client_id = conf['oauth_client_id']
    operator_name = conf['operator_name']

# Downloads the file located at the given URL and saves it to the specified path
def saveUrlToFile(url, path):
    response = requests.get(url, stream=True)
    if response.status_code == 200:
        with open(path, 'wb') as out_file:
            shutil.copyfileobj(response.raw, out_file)
        del response
        return True
    else:
        return False

# Creates the directory structure starting with the path defined in the variable "directory", with subdirs "profile_images" and "cover_images"
# Also creates the log file if log_enabled is true
def setupDirectory(directory):
    global logger
    
    if os.path.isfile(directory):
        print("Unable to create data directory \"" + directory + "\": a file with the same name already exists in that location")
        exit(1)
    if not os.path.isdir(directory):
        try:
            os.mkdir(directory)
        except OSError:
            print("Unable to create data directory \"" + directory + "\" (OS error)")
            exit(1)
    if not os.path.isdir(directory + '/profile_images'):
        try:
            os.mkdir(directory + '/profile_images')
        except OSError:
            print("Unable to create profile_images subdirectory (OS error)")
            exit(1)
    if not os.path.isdir(directory + '/cover_images'):
        try:
            os.mkdir(directory + '/cover_images')
        except OSError:
            print("Unable to create cover_images subdirectory (OS error)")
            exit(1)
    
    # Set up log file handle (creating file if it doesn't exist)
    if log_enabled:
        logpath = directory + '/' + log_file
        try:
            if os.path.isfile(logpath):
                logger = open(logpath, "a")
            else:
                logger = open(logpath, "w+")
                logger.write("---BEGINNING OF PLUSTRACTOR ARCHIVE LOG---\n")
        except IOError as ex:
            print("Error opening log file \"" + logpath + "\" (check permissions or try again with --no-log)")
            exit(2)

# Writes to the log file with a preceding timestamp (if logging is enabled)
def writeLog(message):
    if log_enabled:
        logger.write("[" + getTimeStr() + "] " + message + "\n")

# Prints out the help/usage message
def printHelp():
    helpmsg = '''Usage: plustractor.py [OPTIONS] {ACTION}

ACTION must be exactly one of the following (any arguments passed after an action are ignored):
        
        -a, --activity [Activity ID or URL]
		Scrape a single activity (a post or share) along with all comments under that activity and profile data of all commenters.
                Can supply either the actual ID of the activity object or the full URL for the post, in which case the ID will be looked up (this will take longer).

        --count-acts [Profile ID]
                Counts and prints out the total number of the activities (on Google+, via the API) posted by the given account

        --nametoid [Profile +name]
                Outputs the Person ID of a profile given its short name (the preceding '+' can be omitted)
        
	-p, --profile [Profile ID or +name]
		Download a person's profile data only (does not scrape any activity data). If a record for that profile exists in the database, an option will be given to overwrite it (assuming the profile was found by the API)
        
        --populate-ids      (GDC PROJECT ONLY)
                Populate all of the empty ID profile ID fields on the Google spreadsheet list for the archive project

        --purge [Profile ID or +name]
                Deletes all activities and comments associated with the given person from the database (leaves the person's profile record intact)

        --refresh-acts [Profile ID of person already in database]
                Scrapes all new activity's on the specified profile's feed starting from the most recent, until an activity/post already in the database has been reached.
                The profile must already be present in the database to use this option.

        --rescrape-threads [Date range]
                Re-scrapes all activities in the database that were published in the specified range. (Used mainly to get missing comments.)
                [Date range] must be in the form [start]:[end] where the dates are in YYYY-MM-DD format. If only one date is given, it is treated as the start date while the current date is used as the end date. All date ranges are inclusive.

	-s, --scrape [Profile ID or +name]
        	Scrape a person's profile data along with all activities (posts/shares), comments, and profile data of commenters on activities, going back to the beginning of time.

        --urltoid [Full URL of a G+ post]
                Outputs the Activity ID of a Google+ post given its permalink URL

OPTIONS:

        --apikey [API key]
                Specify a specific Google+ API key to use (otherwise, keys are loaded from ps_keyfile.json)
	
	--date ["MM/DD/YYYY"]
		Used only in conjunction with --profile. Will scrape the specified profile and all activity posted on or after the specified date.
                [UNIMPLEMENTED]
		
	--db [file name, ending in .sqlite (recommended)]
                Specify the name of the database file to be used (found inside the directory specified by the --dir option); set to "plustractor.sqlite" by default. Not recommended to change this unless script is being automated by another script and this option is needed for the project.
        
        --dir [directory name or path]
                Specify the directory under which to save all scraped data (default setting is "gplus_archive/" under the same directory as this script). This directory will contain the database file (named "plustractor.sqlite" by default), and subdirectories for profile and cover images (profile_images and cover_images respectively). If this directory structure doesn't exist, it will be created (however, the directory specified to contain it must already exist). The database file, likewise, will be initialized if it doesn't exist in the specified directory. An error will be thrown if a non-SQLite file with the expected database filename (or one with invalid schema) is found instead.

        --force
                If any record (person, activity, or comment) is found to already exist in the database while scraping, ALWAYS delete that record and insert a fresh copy
	
	-h, --help
		Display usage information (also called when no options are passed)
		
        --quiet
                Supresses most console messages except basic logging information and output of queries (useful for batch profile scraping)
                Overrides -v and --vv

        -v, --verbose
                Print additional status/debug messages
        
        --vv
                Print even more debug messages (was implemented for debugging and shouldn't be used in production; the main thing this does is print details of all requests to the G+ API)
    '''
    print(helpmsg)

# Safely performs shutdown operations and exits with the given code
def exitProg(code = 0):
    db.close()
    if logger != None:
        logger.close()
    exit(code)

### PROGRAM ###
main()
