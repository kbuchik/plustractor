#!/usr/bin/python

import datetime
import gspread
import json
import os
import requests
import subprocess
import sys
from oauth2client.service_account import ServiceAccountCredentials

# G+ scraping project config variables
#dir_name = 'gdc_project_master'
dir_name = 'testdb'
plustractor_path = './plustractor.py'

# GSpread config variables
sheet_key = '1QDEDNTX6gsO9XyXuVvlbQlKjKyNZ1zSrtZzHnzrqiHk'
sheet_name = 'Google+ Archival Project Master Scrapelist'
oauth_keyfile = 'gsheets_client_secret.json'
operator_name = ''
config_file = 'config.json'

gplus_apikey = ''

# Main spreadsheet object (initialized by setupSheets())
doc = None

def main():
    opts = sys.argv
    loadConfig()
    if len(opts) > 1:
        if opts[1] not in ('--help', '-h'):
            setupSheets()
        if opts[1] in ('--help', '-h'):
            printHelp()
            return 1
        if opts[1] == '--mark-scraped':
            if len(opts) < 5:
                print('Usage for this option:\n\tsheetops.py --mark-scraped [Person ID] ["Scraped At"] ["Scraped By"]')
                return 1
            setupSheets()
            markAsScraped(opts[2], opts[3], opts[4])
            return 0
        if opts[1] == '--populate-ids':
            setupSheets()
            populateAllPersonIDs()
            return 0
        if opts[1] == '--scrape-one':
            if len(opts) != 3:
                print("Usage for this option: " + opts[0] + " --scrape-one [person ID]")
                return 1
            idTarget = opts[2]
            if not scrapeOne(idTarget):
                print("ID " + idTarget + " not found in spreadsheet (use plustractor directly or add the profile to the sheet first)")
                return 0
            return 0
        if opts[1] == '--scrape-range':
            if len(opts) != 4:
                print("Usage for this option: " + opts[0] + " --scrape-range [profile ID to start from] [range]")
                return 1
            idTarget = opts[2]
            num = opts[3]
            print("--scrape-range not yet implemented")
            return 0
        else:
            print("Unknown option: " + opts[1])
            return 1
    else:
        print("No operation specified")
    return 0

### FUNCTIONS ###

## Scraping actions

def scrapeOne(persId):
    loc = getIDLocation(persId)
    if not loc:
        return False
    psCommand = plustractor_path + ' --dir ' + dir_name + ' --scrape ' + persId
    psArgs = ' -- dir ' + dir_name + ' --scrape ' + persId
    #proc = subprocess.Popen([plustractor_path, psArgs], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    #for c in iter(lambda: proc.stdout.read(1), b''):
    #    sys.stdout.write(str(c))
    #proc = subprocess.Popen(psCommand, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    #for line in iter(proc.stdout.readline, ''):
    #    print(line)
    #while proc.poll is None:
    #    time.sleep(.1)
    #    err = proc.stderr.read()
    #    if proc.returncode != 0:
    #        print("plustractor.py STDERR: " + str(err))
    return True

def scrapeRange(persId, num):
    pass

## GSheets interaction functions

# Initialize connection to spreadsheet
def setupSheets():
    global doc
    scope = ['https://spreadsheets.google.com/feeds']
    try:
        creds = ServiceAccountCredentials.from_json_keyfile_name(oauth_keyfile, scope)
        client = gspread.authorize(creds)
        doc = client.open_by_key(sheet_key)
    except gspread.exceptions.GSpreadException as ex:
        print("Error connecting to spreadsheet: " + str(ex))
        return False
    return True

# Loads a single G+ API key into variable "gplus_apikey" from the pluscraper config/keyfile (defined by "config_file")
def loadConfig():
    global gplus_apikey,operator_name
    if not os.path.isfile(config_file):
        print("Config file " + config_file + " could not be found")
    conf = json.loads(open(config_file, 'r').read())
    operator_name = conf['operator_name']
    apikeys = conf['apikeys']
    if len(apikeys) == 0 or apikeys[0] == '':
        print("Error loading G+ API keys from file " + config_file)
        exit(1)
    gplus_apikey = apikeys[0]
    return True

# Returns a tuple indicating the location of a given user ID in the doc: (sheet#, row#)
# Returns None if the ID doesn't exist in the spreadsheet
def getIDLocation(query):
    sheetId = 0
    for sh in doc.worksheets():
        rowId = 1
        idCol = sh.col_values(3)
        for pId in idCol:
            if pId == query:
                nameCol = sh.col_values(1)
                return (sheetId, rowId)
            rowId += 1
        sheetId += 1
    return None

# Returns the current date and time in the format YYYY-mm-dd HH:MM:SS
def getTimeStr():
    return datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

# Uses the G+ API to look up a person/profile ID from their short name ("+personName") ("+" prefix will be prepended if not present)
def lookupPersonID(pName):
    if pName[0] != '+':
        pName = '+' + pName
    pJson = plusAPICall('people/' + pName)
    if pJson == '404':
        return ''
    else:
        return json.loads(pJson)['id']

# Fills in the "Scraped At" and "Scraped By" columns in the row for the given person ID
def markAsScraped(persId, scrapedAt, scrapedBy="", activityCount=0):
    loc = getIDLocation(persId)
    if loc == None:
        return False
    sheet = doc.get_worksheet(loc[0])
    sheet.update_cell(loc[1], 5, scrapedAt)
    sheet.update_cell(loc[1], 6, scrapedBy)
    sheet.update_cell(loc[1], 7, activityCount)
    name = sheet.cell(loc[1], 1).value
    print('Marked "' + name + '" as scraped')
    return True

# Iterate through all rows and populate the ID column whenever an empty ID cell is found
# (either from the given URL, or by looking up the name with the G+ API)
def populateAllPersonIDs():
    print("Populating all empty person ID fields")
    for shId in range(0,3):
        sh = doc.get_worksheet(shId)
        nameCol = sh.col_values(1)
        urlCol = sh.col_values(2)
        idCol = sh.col_values(3)
        for rowId in range(0, len(nameCol)):
            persId = ''
            if rowId < len(idCol):
                persId = idCol[rowId]
            if rowId == 0:
                continue
            if persId == '':
                urlSuffix = urlCol[rowId].split('/')[3]
                if urlSuffix[0] != '+':
                    print("Populating ID entry for \"" + nameCol[rowId] + "\" with ID from URL: " + urlSuffix)
                    sh.update_cell(rowId + 1, 3, urlSuffix)
                else:
                    newId = lookupPersonID(urlSuffix)
                    if newId != '':
                        print("Populating ID entry for \"" + nameCol[rowId] + "\" with new ID: " + newId)
                        sh.update_cell(rowId + 1, 3, newId)
                    else:
                        print("Unable to get new ID for person \"" + nameCol[rowId] + "\"")

## G+ interaction functions

# Stripped-down version of plusAPICall() from pluscraper.py
def plusAPICall(call, prm={}):
    if gplus_apikey == '':
        loadConfig()
    url = 'https://www.googleapis.com/plus/v1/' + call
    prm['key'] = gplus_apikey
    req = requests.get(url, params=prm)
    if req.status_code == 200:
        return req.text
    elif req.status_code == 404:
        return '404'
    else:
        print("Google+ API request failed with status code " + str(req.status_code))
        print("Request URL: " + url)
        print("Request response:")
        print(req.text)
        exit(2)
    return ''

# Print program help
def printHelp():
    print("Usage: autoscrape.py [ACTION]\n")
    print("Actions:")
    print("\t--help")
    print("\t--mark-scraped [person ID] [\"Scraped at\"] [\"Scraped by\"]")
    print("\t--populate-ids")
    print("\t--scrape-one [person ID]")
    print("\t--scrape-range [person ID to start from, inclusive] [range]")

# Execute main function
exit(main())
